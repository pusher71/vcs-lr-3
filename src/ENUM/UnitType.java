public enum UnitType {
    NONE,
    SHIP_PART,
    BROKEN_SHIP_PART,
    NAVAL_MINE,
    ISLAND,
    LIGHTHOUSE
}
